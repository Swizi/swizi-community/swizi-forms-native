const webpack = require("webpack");
const path = require("path");

const config = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
            plugins: [require("@babel/plugin-proposal-object-rest-spread")],
          },
        },
      },
    ],
  },
  resolve: {
    extensions: [".js"],
  },
  plugins: [
    new webpack.DefinePlugin({
      __VERSION__: JSON.stringify(process.env.npm_package_version)
    })
  ]
};

module.exports = [
  {
    entry: path.resolve(__dirname, "src/forms.js"),
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "swizi-forms-native.js",
      library: 'swizi-forms-native',
      libraryTarget: "umd",
      umdNamedDefine: true,
    },
    mode: "development",
    module: config.module,
    resolve: config.resolve,
    plugins: config.plugins,
  },
  {
    entry: path.resolve(__dirname, "src/forms.js"),
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "swizi-forms-native.min.js",
      library: 'swizi-forms-native',
      libraryTarget: "umd",
      umdNamedDefine: true,
    },
    mode: "production",
    module: config.module,
    resolve: config.resolve,
    plugins: config.plugins,
  },
];
