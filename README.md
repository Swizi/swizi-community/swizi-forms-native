# Swizi Forms Native SDK

This SDK allows to use Swizi Forms service from anywhere in a Swizi app. 

**Disclaimer: at the moment, only plugin sections support connecting a Swizi Forms datasource.**

When a Swizi Forms datasource is connected to a section, a background process will be executed for this datasource (meaning that if the same datasource is connected to multiple sections, one Forms datasource will be shared between the sections) when launching the application and will fetch datas and process requests automatically. A cache is also provided, so you won't need to think about it.

## Installation

    npm i swizi-forms-native
    
or

    yarn add swizi-forms-native
    
## Usage

The usage of the SDK is pretty simple.

First import:
    
    import Forms from 'swizi-forms-native'
    
Then, use methods. No need to give any identification keys, the Swizi Forms datasource already got it.

## Methods

Almost all methods are returning promises. So you'll get returned datas in the then().

### createView

    Forms.createView(viewName, formName, criteria).then(datas => {})
    
### refreshView

    Forms.refreshView(viewName).then(datas => {})
    
### filterView

    Forms.filterView(viewName, includeCriteria, excludeCriteria).then(datas => {})
    
### createOrRefreshView

    Forms.createOrRefreshView(viewName, formName, criteria).then(datas => {})
    
### exists

    Forms.exists(viewName).then(datas => {})
    
### createRecord

    Forms.createRecord(queryName, formName, fields).then(datas => {})
    
### updateRecord

    Forms.updateRecord(queryName, formName, criteria, data).then(datas => {})
    
### clearCache

    Forms.clearCache().then(datas => {})
    
### addListener

    Forms.addListener(listener)
    
### getView

    Forms.getView(viewName).then(datas => {})
    
### getData

    Forms.getData(viewName).then(datas => {})
    
### getPendingQuery

    Forms.getPendingQuery(type).then(datas => {})

### clearPendingQuery

    Forms.clearPendingQuery(type).then(datas => {})
    
### getUpdatedAt
    
    Forms.getUpdatedAt().then(datas => {})

### increment

    Forms.increment(formName, recordId, fieldName, emitter).then(datas => {})

### decrement

    Forms.decrement(formName, recordId, fieldName, emitter).then(datas => {})