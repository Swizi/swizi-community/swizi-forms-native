var version = __VERSION__;
var isIOS = false;
var formIOSBridge;
var promises = {};

if (typeof plugins === "undefined") {
  if (
    window.webkit &&
    window.webkit.messageHandlers &&
    window.webkit.messageHandlers.formsPlugin
  ) {
    formIOSBridge = window.webkit.messageHandlers.formsPlugin;
    isIOS = true;
  }
}

/**
 * Forms 
 */

var forms = {};
forms.listener = [];

forms.generateUUID = () => {
  var d = new Date().getTime();
  var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(
    c,
  ) {
    var r = ((d + Math.random() * 16) % 16) | 0;
    d = Math.floor(d / 16);
    return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
  });
  return uuid;
};

forms.resolvePromise = (promiseId, data, error) => {
  if (error) {
    promises[promiseId].reject(data);
  } else {
    promises[promiseId].resolve(data);
  }
  delete promises[promiseId];
};

forms.call = (method, args) => {
  var promise = new Promise(function(resolve, reject) {
    var promiseId = forms.generateUUID();
    promises[promiseId] = { resolve: resolve, reject: reject };

    try {
      formIOSBridge.postMessage({
        promiseId: promiseId,
        method: method,
        args: args || [],
      });
    } catch (exception) {
      alert(exception);
    }
  });
  return promise;
};

forms.isFormsBridgeReady = () => {
  if (typeof formIOSBridge != "undefined" || typeof formsPlugin != "undefined")
    return true;
  else return false;
};

forms.formNotify = (queryNames, eventType) => {
  if (!forms.listener) {
    return
  }
  forms.listener.forEach(listener => {
    listener.call(queryNames, eventType)
  })
};

forms.createView = (viewName, formName, criteria, emitter, withParentId) => {
  if (criteria == undefined){
    criteria = null
  }

  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("createView", [viewName, formName, criteria, emitter, withParentId]).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.createView(viewName, formName, criteria != null ? JSON.stringify(criteria) : criteria, emitter, withParentId, promiseId);
      });

      return promise
    }
  } else {
    return null;
  }
};

forms.refreshView = (viewName) => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("refreshView", [viewName]).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.refreshView(viewName, promiseId)
      });

      return promise
    }
  } else {
    return null;
  }
};

forms.filterView = (viewName, includeCriteria, excludeCriteria) => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("filterView", [viewName, includeCriteria, excludeCriteria]).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.filterView(viewName, includeCriteria != null ? JSON.stringify(includeCriteria) : includeCriteria, excludeCriteria != null ? JSON.stringify(excludeCriteria) : excludeCriteria, promiseId);
      });

      return promise
    }
  } else {
    return null;
  }
};

forms.createOrRefreshView = (viewName, formName, criteria, emitter, withParentId) => {
  if (criteria == undefined){
    criteria = null
  }

  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("createOrRefreshView", [viewName, formName, criteria, emitter, withParentId]).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.createOrRefreshView(viewName, formName, criteria != null ? JSON.stringify(criteria) : criteria, emitter, withParentId, promiseId);
      });

      return promise
    }
  } else {
    return null;
  }
};

forms.exists = viewName => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("exists", [viewName]).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.exists(viewName, promiseId);
      });

      return promise
      //return form.exists(viewName);
    }
  } else {
    return null;
  }
};

forms.createRecord = (queryName, formName, fields) => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("createRecord", [queryName, formName, fields]).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.createRecord(queryName, formName, JSON.stringify(fields), promiseId);
      });

      return promise
    }
  } else {
    return null;
  }
};

forms.updateRecord = (queryName, formName, criteria, data) => {
  if (criteria == undefined){
    criteria = null
  }

  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("updateRecord", [queryName, formName, criteria, data]).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.updateRecord(queryName, formName, JSON.stringify(criteria), JSON.stringify(data), promiseId);
      });

      return promise
    }
  } else {
    return null;
  }
};

forms.clearCache = () => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("clearCache", null).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.clearCache(promiseId);
      });

      return promise
    }
  } else {
    return null;
  }
};

forms.addListener = listener => {
  if (forms.isFormsBridgeReady()) {
    var name = "form_listener_"+Math.floor((Math.random() * 100000));
    let element = {name : name, call : listener};
    forms.listener.push(element);
  } else {
    return null;
  }
};

forms.getView = viewName => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("getView", [viewName]).then(function(result) {
        return JSON.parse(result);
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.getView(viewName, promiseId)
      });

      return promise

      //let result = formsPlugin.getView(viewName);
      //return JSON.parse(result)
    }
  } else {
    return null;
  }
};

forms.getData = viewName => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("getData", [viewName]).then(function(result) {
        return JSON.parse(result);
      });
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.getData(viewName, promiseId)
      });

      return promise

      //let result = formsPlugin.getData(viewName);
      //return JSON.parse(result)
    }
  } else {
    return null;
  }
};

forms.getPendingQuery = type => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("getPendingQuery", [type]).then(function(result) {
        return JSON.parse(result);
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.getPendingQuery(type, promiseId)
      });

      return promise
      
      //let result = formsPlugin.getPendingQuery(type);
      //return JSON.parse(result)
    }
  } else {
    return null;
  }
};

forms.clearPendingQuery = type => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("clearPendingQuery", [type]).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.clearPendingQuery(type, promiseId)
      });

      return promise
      //formsPlugin.clearPendingQuery(type);
    }
  } else {
    return null;
  }
};

forms.getUpdatedAt = () => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("getUpdatedAt", null).then((results) => {
        return JSON.parse(results)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.getUpdatedAt(promiseId)
      });

      return promise

      //return formsPlugin.getUpdatedAt();
    }
  } else {
    return null;
  }
};

forms.setBadge = (badgeNumber) => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("setBadge", [badgeNumber]).then((result) => {
        return JSON.parse(result)
      })
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.setBadge(badgeNumber, promiseId)
      });

      return promise
    }
  }
}

forms.increment = (formName, recordId, fieldName, emitter) => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("increment", [formName, recordId, fieldName, emitter]).then(function(result) {
        return JSON.parse(result);
      });
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.incrementField(formName, recordId, fieldName, emitter, promiseId)
      });

      return promise
    }
  } else {
    return null;
  }
};

forms.decrement = (formName, recordId, fieldName, emitter) => {
  if (forms.isFormsBridgeReady()) {
    if (isIOS) {
      return forms.call("decrement", [formName, recordId, fieldName, emitter]).then(function(result) {
        return JSON.parse(result);
      });
    } else {
      var promise = new Promise(function(resolve, reject) {
        
        var promiseId = forms.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };

        formsPlugin.decrementField(formName, recordId, fieldName, emitter, promiseId)
      });

      return promise
    }
  } else {
    return null;
  }
};

document.documentElement.dataset.forms = true;
document.documentElement.dataset.formsVersion = version;

window.forms = forms;

export default forms;